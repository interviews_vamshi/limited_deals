/** Instructions **/
npm install
npm start


/** End Points **/

Create A Deal
1. POST http://localhost:3000/api/v0/e-commerce/deals 
{
    "name": "Lightening Deals3",
    "prodName": "Sony 2 Headphones",
    "duration": 20000,
    "allowedPurchases": 5
}

Get Deals List
2. GET http://localhost:3000/api/v0/e-commerce/deals 

Purchase A Deal
3. POST http://localhost:3000/api/v0/e-commerce/purchases
{
    "userId": "u-12345678",
    "dealId": "613a02db3c5682551d25c348"
}