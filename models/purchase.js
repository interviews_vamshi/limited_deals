const mongoose = require('mongoose');
const purchaseSchema = new mongoose.Schema({
    user_id: 'string', //user who made the purchase
    deal_id: 'string', //unique id of the deal
});

const Purchase = mongoose.model('purchase', purchaseSchema);
module.exports = Purchase