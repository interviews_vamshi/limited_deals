const mongoose = require('mongoose');

const dealSchema = new mongoose.Schema({
    name: 'string',
    prod_id: 'string',
    start_time: { type: Date, default: Date.now }, //when the deal starts
    duration: Number, //Time in seconds
    allowed_purchases: Number, //total number of items or units allowed to purchase as part of deal
    purchases: Number,
});

const Deal = mongoose.model('deal', dealSchema);

module.exports = Deal