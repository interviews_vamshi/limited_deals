const dealModel = require('../../models/deal');

const createNewDeal = async (body) => {
    const newDealObj = {
        name: body.name,
        prod_id: body.prodName,
        duration: parseInt(body.duration), //duration for which the deal stays active
        allowed_purchases: parseInt(body.allowedPurchases), //allowed purchases as part of deal
        purchases: 0, //initially no purchases are made
    }

    await dealModel.create(newDealObj);
}

const getDealsList = async () => {
    const deals = await dealModel.find();
    return deals;
}

const updateDeal = async (dealId, updateObj) => {
    let updateDoc = {};
    if (updateObj.allowedPurchases) {
        updateDoc.allowed_purchases = parseInt(updateObj.allowedPurchases);
    }
    if (updateObj.duration) {
        updateDoc.duration = parseInt(updateObj.duration);
    }

    let query = {
        "_id": dealId
    }
    await dealModel.findOneAndUpdate(query, updateDoc);
}

module.exports = {
    createNewDeal,
    getDealsList,
    updateDeal
}