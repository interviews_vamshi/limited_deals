var express = require('express');
var router = express.Router();
const controller = require("./controller");
/**
 * Deals Router
 */
router.post('/', controller.createDeal);

router.put('/:dealId', controller.updateDeal);

router.get('/', controller.getDealsList);

module.exports = router;
