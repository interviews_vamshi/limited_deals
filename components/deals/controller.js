const service = require('./service');

async function getDealsList(req, res, next) {
    try {
        let deals = await service.getDealsList();
        res.status(200).send({
            deals: deals
        })
    } catch (error) {
        console.log(error);
        /**
         * TO DO: Error hanbdling for duplicate entries, Unprocessable entities
         */
        return res.status(500).send({
            "error": "Internal error in fetching deals"
        })
        
    }
}

async function createDeal(req, res, next) {
    try {
        await service.createNewDeal(req.body);
        return res.status(201).send({
            "msg": "New Deal Created Succesfully."
        });
    } catch (error) {
        console.log(error);
        /**
         * TO DO: Error hanbdling for duplicate entries, Unprocessable entities
         */
        return res.status(500).send({
            "error": "Internal error in creating deal"
        })
    }
}

async function updateDeal(req, res, next) {
    try {
        await service.updateDeal(req.params.dealId, req.body);
        return res.status(201).send({
            "msg": "Deal Updated Succesfully."
        });
    } catch (error) {
        console.log(error);
        /**
         * TO DO: Error hanbdling for duplicate entries, Unprocessable entities
         */
        return res.status(500).send({
            "error": "Internal error in updating deal"
        })
    }
}

module.exports = {
    getDealsList,
    createDeal,
    updateDeal
}