const service = require('./service');

async function makePurchase(req, res, next) {
    try {
        let response = await service.makePurchase(req.body);
        return res.status(response.code).send({msg: response.msg});
    } catch (error) {
        console.log(error);
        /**
         * TO DO: Error hanbdling for duplicate entries, Unprocessable entities
         */
        return res.status(500).send({
            "error": "Internal error in updating deal"
        })
    }
}

module.exports = {
    makePurchase
}