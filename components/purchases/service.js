const purchaseModel = require('../../models/purchase');
const dealModel = require("../../models/deal");

const makePurchase = async (body) => {
    let userId = body.userId;
    let dealId = body.dealId;
    let purchases = await purchaseModel.find({user_id: userId, deal_id: dealId});
    if (purchases.length > 0) {
        return {
            "code": 409,
            "msg": "Conflct, deal already purchased"
        }
    }

    let deal  = await dealModel.findById({"_id": dealId});
    if (deal && deal.allowed_purchases === deal.purchases) {
        return {
            "code": 403,
            "msg": "Deal closed"
        }
    }

    let currentTime  = Date.now()/1000;
    let dealStartTime = deal.start_time.getTime()/1000;
    let timeElasped = currentTime -  dealStartTime;
    if (timeElasped < deal.duration) {
        const newPurchaseObj = {
            user_id: userId,
            deal_id: dealId
        }
    
        await purchaseModel.create(newPurchaseObj);

        let updateDoc = {
            purchases: deal.purchases + 1
        };
        let query = {
            "_id": dealId
        }
        await dealModel.findOneAndUpdate(query, updateDoc);

        return {
            "code": 201,
            "msg": "Deal Purchased Successfully"
        }
    } else {
        return {
            "code": 403,
            "msg": "Deal closed"
        }
    }
}

module.exports = {
    makePurchase
}