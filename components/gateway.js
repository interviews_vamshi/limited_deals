const dealRouter = require('../components/deals/router');
const purchaseRouter = require('../components/purchases/router');
const baseUrl = "/api/v0/e-commerce"


function gateWay(app) {
    console.log("gateway");
    app.use(`${baseUrl}/deals`, dealRouter);
    app.use(`${baseUrl}/purchases`, purchaseRouter);
}

module.exports = gateWay;